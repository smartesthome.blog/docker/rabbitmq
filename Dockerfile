FROM rabbitmq:3.7.17-alpine

ARG default_user=iulius
ARG default_password=iulius

ENV RABBITMQ_DEFAULT_USER=$default_user
ENV RABBITMQ_DEFAULT_PASS=$default_password

COPY conf/enabled_plugins /etc/rabbitmq/enabled_plugins

COPY conf/rabbitmq.config /etc/rabbitmq/rabbitmq.conf

EXPOSE 15672 61613